\babel@toc {spanish}{}
\contentsline {part}{I\hspace {1em}Generalidades del proyecto}{5}{part.1}%
\contentsline {chapter}{Descripción del problema}{7}{chapter*.2}%
\contentsline {chapter}{Planteamiento del problema}{9}{chapter*.3}%
\contentsline {chapter}{Objetivos}{11}{chapter*.4}%
\contentsline {section}{Objetivos generales}{11}{section*.5}%
\contentsline {section}{Objetivos específicos}{11}{section*.6}%
\contentsline {chapter}{Hipótesis y supuestos}{13}{chapter*.7}%
\contentsline {chapter}{Justificación}{15}{chapter*.8}%
\contentsline {part}{II\hspace {1em}Marco Teórico}{17}{part.2}%
\contentsline {chapter}{Antecedentes o marco histórico}{19}{chapter*.9}%
\contentsline {section}{\textbf {Sobre los temas tratados}}{19}{section*.10}%
\contentsline {subsection}{La ``Trie''}{19}{subsection*.11}%
\contentsline {subsection}{La memoización}{19}{subsection*.12}%
\contentsline {subsection}{La programación dinámica}{19}{subsection*.13}%
\contentsline {subsection}{El lenguaje de programación C}{20}{subsection*.14}%
\contentsline {section}{\textbf {Contribuciones a la enseñanza}}{21}{section*.15}%
\contentsline {chapter}{Marco conceptual}{23}{chapter*.16}%
\contentsline {section}{Estructura de datos}{23}{section*.17}%
\contentsline {section}{Algoritmo}{24}{section*.18}%
\contentsline {section}{Big O Notation}{24}{section*.19}%
\contentsline {section}{Optimización}{24}{section*.20}%
\contentsline {section}{Naive implementation}{25}{section*.21}%
\contentsline {chapter}{Marco referencial}{27}{chapter*.22}%
\contentsline {section}{Reseñando métodos de enseñanza y obras}{28}{section*.23}%
\contentsline {section}{Enseñanza gamificada}{29}{section*.24}%
\contentsline {part}{III\hspace {1em}Metodología}{31}{part.3}%
\contentsline {chapter}{Población o universo/muestra}{33}{chapter*.25}%
\contentsline {chapter}{Tipo de estudio}{35}{chapter*.26}%
\contentsline {chapter}{Descripción del Instrumento}{37}{chapter*.27}%
\contentsline {chapter}{Procedimiento de recolección}{39}{chapter*.28}%
\contentsline {section}{Diseñando las preguntas}{39}{section*.29}%
\contentsline {paragraph}{Pregunta 1}{39}{paragraph*.30}%
\contentsline {paragraph}{Pregunta 2}{39}{paragraph*.31}%
\contentsline {paragraph}{Pregunta 3}{39}{paragraph*.32}%
\contentsline {paragraph}{Pregunta 4}{39}{paragraph*.33}%
\contentsline {paragraph}{Pregunta 5}{40}{paragraph*.34}%
\contentsline {paragraph}{Pregunta 6}{40}{paragraph*.35}%
\contentsline {paragraph}{Pregunta 7}{40}{paragraph*.36}%
\contentsline {paragraph}{Pregunta 8}{40}{paragraph*.37}%
\contentsline {paragraph}{Pregunta 9}{40}{paragraph*.38}%
\contentsline {paragraph}{Pregunta 10}{40}{paragraph*.39}%
\contentsline {paragraph}{Pregunta 11}{41}{paragraph*.40}%
\contentsline {paragraph}{Pregunta 12}{41}{paragraph*.41}%
\contentsline {paragraph}{Pregunta 13}{41}{paragraph*.42}%
\contentsline {paragraph}{Pregunta 14}{41}{paragraph*.43}%
\contentsline {paragraph}{Pregunta 15}{42}{paragraph*.44}%
\contentsline {section}{Problema de Advent of Code}{42}{section*.45}%
\contentsline {section}{Obteniendo el dataset}{42}{section*.46}%
\contentsline {part}{IV\hspace {1em}Resultados y conclusiones}{43}{part.4}%
\contentsline {chapter}{Resultados obtenidos y discusión}{45}{chapter*.47}%
\contentsline {section}{\textbf {Resultados de las preguntas cuantificables}}{45}{section*.48}%
\contentsline {subsection}{Pregunta 1}{45}{subsection*.49}%
\contentsline {subsection}{Pregunta 2}{46}{subsection*.50}%
\contentsline {subsection}{Pregunta 3}{46}{subsection*.51}%
\contentsline {subsection}{Pregunta 4}{47}{subsection*.52}%
\contentsline {subsection}{Pregunta 7}{47}{subsection*.53}%
\contentsline {subsection}{Pregunta 9}{48}{subsection*.54}%
\contentsline {subsection}{Pregunta 10}{48}{subsection*.55}%
\contentsline {subsection}{Pregunta 14}{49}{subsection*.56}%
\contentsline {subsection}{Pregunta 15}{49}{subsection*.57}%
\contentsline {section}{\textbf {Resultados de las preguntas abiertas}}{50}{section*.58}%
\contentsline {subsection}{Pregunta 5}{50}{subsection*.59}%
\contentsline {subsection}{Pregunta 6}{51}{subsection*.60}%
\contentsline {subsection}{Pregunta 8}{51}{subsection*.61}%
\contentsline {subsection}{Pregunta 11}{52}{subsection*.62}%
\contentsline {subsection}{Pregunta 12}{52}{subsection*.63}%
\contentsline {subsection}{Pregunta 13}{52}{subsection*.64}%
\contentsline {section}{\textbf {Resultados de la prueba de código}}{53}{section*.65}%
\contentsline {subsection}{Implementación ingenua en JavaScript}{53}{subsection*.66}%
\contentsline {subsection}{Implementación con diccionarios en Haskell}{55}{subsection*.67}%
\contentsline {subsection}{\textbf {Implementación en C++ con una \textit {Lookup Table}}}{56}{subsection*.68}%
\contentsline {chapter}{Conclusiones y recomendaciones}{57}{chapter*.69}%
\contentsline {section}{Recomendaciones}{58}{section*.70}%
\contentsline {chapter}{Bibliograf\'{\i }a}{59}{chapter*.71}%
\contentsfinish 

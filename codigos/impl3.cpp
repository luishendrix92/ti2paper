#include <stdio.h>

int main() {
  const int nthTurn = 2020;
  const int inputSize = 6;
  int input[inputSize] = {2,0,6,12,1,3};
  static int lut[nthTurn];
  int lastSpoken = input[inputSize - 1];

  for (int i = 0; i < inputSize; i++)
    lut[input[i]] = i + 1;

  for (int turn = inputSize+1; turn <= nthTurn; turn++) {
    int newlySpoken = lut[lastSpoken] != 0
      ? turn - lut[lastSpoken] - 1 : 0;

    lut[lastSpoken] = turn - 1;
    lastSpoken = newlySpoken;
  }

  printf("%d\n", lastSpoken);

  return 0;
}

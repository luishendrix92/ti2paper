const arr = require('fs')
  .readFileSync("input.txt", "utf8")
  .split(",")
  .map(e => Number(e));
const len = arr.length;

function nextNumber(counter = 0) {
    if (counter == (30000000 - len)) {
      console.log(arr[arr.length - 1]);
    } else {
        const index = arr.slice(arr, arr.length - 1)
          .lastIndexOf(arr[arr.length - 1]);

        if (index > -1) {
            arr.push(arr.length - index - 1);
        } else {
            arr.push(0);
        }

        return nextNumber(counter + 1);
    }
}

nextNumber();


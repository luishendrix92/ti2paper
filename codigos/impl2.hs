import qualified Data.Map.Strict as Map

solve limit mem t prev 
  | t == limit = prev
  | otherwise =
    case Map.lookup prev mem of
      Nothing -> solve limit (Map.insert prev (t-1) mem) (t+1) 0
      Just t' -> solve limit (Map.insert prev (t-1) mem) (t+1) (t-t'-1)

main = do 
  let input = [2,0,6,12,1,3]
      initMemory = Map.fromList $ zip (init input) [1..]
  print $ solve 30000001 initMemory (length input+1) (last input)

